"""
Tests for the models.
"""

import pytest
import sqlalchemy.orm

import bluedragonfly.database.models as models

@pytest.mark.unit
def test_password_change():
    """
    It should not be possible to store users password in plain text.
    """

    # Using a db in memory.
    engine = sqlalchemy.create_engine('sqlite:///:memory:', echo=False)
    models.Base.metadata.create_all(engine)

    user = models.User(fullname='Dumb LeTont', email='leTont@yahoo.com', password='12345')
    assert user.password != '12345', 'The password can be set as plaintext!'
    assert user.check_password('12345'), 'Cannot verify password!'