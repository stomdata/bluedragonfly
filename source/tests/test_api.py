"""
Test api operations.
"""

import pandas as pd
import pytest
import requests

from .test_db import create_db_session
from .test_basic_api import api_url
from bluedragonfly.database.database import DB as db
from bluedragonfly.database.models import Sensor, Value, Greenhouse
from datetime import datetime
import logging

@pytest.fixture
def create_temp_gh(api_url):
    with db.session_scope(db.Session) as session:
        gh2 = Greenhouse(name='gh2')
        session.add(gh2)
        session.add(Sensor(uid='temperature', unit='C', greenhouse=gh2))
        session.add(Sensor(uid='humidity', unit='%', greenhouse=gh2))
        session.commit()

@pytest.mark.integration
def test_get_data(create_db_session, api_url):

    # MG TODO
    # the following test assumes that the database contains data for sensor with a given name,
    # between the specified period. This is so with the current db.sqlite, but will break in the future!

    req = requests.get(api_url + '/api/data?sensor=%s&start=%s&end=%s' % ('sensor1.Air.T', '2017-12-25T00:00', '2017-12-26T00:00'))
    assert req.status_code == 200, "API did returned a status code of %s instad of 200." % req.status_code

    df = pd.DataFrame.from_dict(req.json(), orient='index')
    df.index = pd.to_datetime(df.index, unit='ms')

    assert not df.empty, "Empty df in the response."

@pytest.mark.integration
def test_post_data(api_url):

    data = pd.DataFrame({'value' : [1.0,2.0],
                         'uid' : ['temperature', 'humidity'],
                         'greenhouse' : ['gh.beta', 'gh.beta'],
                         'unit' : ['C', 'C']
                        }, index = pd.to_datetime(['2018-01-01', '2018-01-02'] ))

    req = requests.post(api_url + '/api/data', json=data.to_json(orient='index'))
    assert req.status_code == 200, "API did returned a status code of %s instad of 200." % req.status_code
    assert req.json() == 'ok', "API did not respond properly."
