"""
Testing basic interactions with the API.
"""

import os
import logging
import re
from multiprocessing import Process
import requests
from time import sleep
import pytest

@pytest.fixture
def api_url():
    try:
        API = os.environ['API']
    except KeyError:
        raise RuntimeError('\'API\' environmental variable not defined.')
    return API

@pytest.mark.integration
def test_api_up_and_running(api_url):
    """
    check that no errors occur when we provide good input
    """
    req = requests.get(api_url)
    assert req.status_code == 200, "API returned status code %s" %req.status_code

