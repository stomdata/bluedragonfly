"""
Test authentication against the API.
"""

import requests
import logging
import pytest

from .test_basic_api import api_url

@pytest.mark.unit
def test_unsuccessful_login(api_url):
    req = requests.get('%s/auth/test' % api_url, auth=('user', 'pass'))
    assert req.status_code == 401, "API did returned a status code of %s instad of 401." % req.status_code

    req = requests.get('%s/auth/token' % api_url, auth=('user', 'pass'))
    assert req.status_code == 401, "API did returned a status code of %s instad of 401." % req.status_code

@pytest.mark.unit
def test_successful_login(api_url):
    req = requests.get('%s/auth/test' % api_url, auth=('Dummy LeTont', 'dummy_letont'))
    assert req.status_code == 200, "API did returned a status code of %s instad of 200." % req.status_code
    assert req.content == b"Success", "API returned %s" % req.content

@pytest.mark.unit
def test_token(api_url):
    req = requests.get('%s/auth/token' % api_url, auth=('Dummy LeTont', 'dummy_letont'))
    assert req.status_code == 200, "API did returned a status code of %s instad of 200." % req.status_code
    token = req.json()['token']

    req = requests.get('%s/auth/test' % api_url, auth=(token, 'dummy_letont'))
    assert req.status_code == 200, "API did returned a status code of %s instad of 200." % req.status_code
    assert req.content == b"Success", "API returned %s" % req.content