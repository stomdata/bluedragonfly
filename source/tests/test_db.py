from datetime import datetime
import pandas as pd
from time import sleep
import pytest
import sqlalchemy

from bluedragonfly.database.database import DB, session_scope
from bluedragonfly.database.models import Greenhouse, User, Role, SensorValue, Sensor, Base

@pytest.fixture
def create_db_session():
    engine = sqlalchemy.create_engine('sqlite:///:memory:', echo=False)
    Base.metadata.create_all(engine)
    Session = sqlalchemy.orm.sessionmaker(bind=engine)
    return Session

@pytest.mark.unit
def test_retrieve_sensor_values(create_db_session):
    Session = create_db_session

    with session_scope(Session) as session:
        s1 = Sensor(uid='s1', unit='C', location='room1')
        s1.values.append(SensorValue(datetime=datetime(2018, 1, 1), value=1))
        s1.values.append(SensorValue(datetime=datetime(2018, 1, 2), value=2))
        session.add(s1)
        session.commit()

    db = DB()

    with session_scope(Session) as session:
        df = db.retrieve_sensor_data(session=session, sensor_uid='s1')

    reference = pd.DataFrame({ 'value' : [1.0,2.0],
                               'uid' : ['s1', 's1'],
                               'unit' : ['C', 'C']
                            }, index = pd.to_datetime(['2018-01-01', '2018-01-02'] ))

    assert reference.equals(df)

@pytest.mark.unit
def test_appending_data(create_db_session):
    Session = create_db_session

    data1 = pd.DataFrame({ 'value' : [1.0,2.0],
                           'uid' : ['s1', 's2'],
                           'unit' : ['C', 'C']
                         }, index = pd.to_datetime(['2018-01-01', '2018-01-02'] ))

    data2 = data1.copy()
    data2.index = pd.to_datetime(['2018-01-03', '2018-01-04'] )

    db = DB()

    with session_scope(Session) as session:
        session.add(Sensor(uid='s1', unit='C', location='room1'))
        session.add(Sensor(uid='s2', unit='C', location='room1'))
        session.commit()

        db.append_sensor_data(session, data1)
        db.append_sensor_data(session, data2)

    with session_scope(Session) as session:
        df = db.retrieve_sensor_data(session=session, sensor_uid='s1')

    reference = pd.DataFrame({ 'value' : [1.0,1.0],
                               'uid' : ['s1', 's1'],
                               'unit' : ['C', 'C']
                            }, index = pd.to_datetime(['2018-01-01', '2018-01-03'] ))

    assert reference.equals(df)

# MG TODO
@pytest.mark.unit
def test_happy_workflow(create_db_session):

    Session = create_db_session
    # create a session
    session = Session()

    # create a user
    admin = Role(name='admin')
    user = User(fullname='User', email='user@example.nl', password='1234')
    # create a greenhouse
    gh1 = Greenhouse(name='gh1')
    # create some sensors
    s1 = Sensor(uid='s1', unit='C', location='room1')
    s2 = Sensor(uid='s2', unit='C', location='room2')

    # add some values from the sensors
    s1.values.append(SensorValue(datetime=datetime(2018, 1, 1), value=3.0))
    s1.values.append(SensorValue(datetime=datetime(2018, 1, 2), value=4.0))
    s2.values.append(SensorValue(datetime=datetime(2018, 1, 2), value=5.0))

    # assign gh1 to matteo
    user.greenhouses.append(gh1)
    user.roles.append(admin)

    # add everything to the session and commit it
    session.add(user)
    session.add(gh1)
    session.add(s1)
    session.add(s2)
    session.commit()

    # retrieve all users.
    for instance in session.query(User).all():
        assert instance.fullname == "User"

    # retrieve all greenhouses.
    for instance in session.query(Greenhouse).order_by(Greenhouse.id):
        assert instance.users == [user]

    # retrieve and assign the sensors to gh1
    gh = session.query(Greenhouse).filter_by(name='gh1').first()
    gh.sensors.extend([s1, s2])

    session.commit()

    # retrieve all greenhouses.
    for sensor in session.query(Sensor).filter(Sensor.uid == 's1'):
        assert [s.value for s in sensor.values] == [3.0, 4.0]

    for _, entry in session.query(Sensor, SensorValue).filter(Sensor.uid == 's1', Sensor.uid == SensorValue.sensor_id, SensorValue.datetime <= datetime(2018, 1, 1)):
        assert [s.value for s in entry.values] == [3.0]
