import os

from pathlib import Path
ROOT =  Path(__file__).parents[4]

# The password used to send mails is not stored in the git repository;
# it is rather stored on a file which is local to the server.
try:
    from .local_settings import MAIL_APP_PASSWORD
except ModuleNotFoundError:
    MAIL_APP_PASSWORD = ''

class Config(object):
    DEBUG = False
    TESTING = False
    SECRET_KEY = 'gnjdksfngoih' #os.urandom(32)
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 465
    MAIL_USE_SSL = True
    MAIL_USERNAME = 'bluedragonfly.service@gmail.com'
    MAIL_PASSWORD = MAIL_APP_PASSWORD
    MAIL_DEFAULT_SENDER : 'mail@bluedragonfly.com'

class ProductionConfig(Config):
    DATABASE_URI = 'mysql://user@localhost/foo'
    SHOW_QUERIES = False

class DevelopmentConfig(Config):
    ENV = 'development'
    DEBUG = True
    DATA = ROOT / 'data'
    DB_PATH = DATA / 'db.sqlite3'
    DATABASE_URI = 'sqlite:///'+ DB_PATH.as_posix()
    SHOW_QUERIES = True

class TestingConfig(Config):
    ENV = 'production'
    TESTING = True
    DEBUG = True
    DATABASE_URI = 'sqlite:///:memory:'
    SHOW_QUERIES = True
    DATABASE_URI = 'sqlite:///:memory:'
