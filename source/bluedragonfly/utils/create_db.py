import numpy as np

from pathlib import Path
from bluedragonfly.database.models import Base, User, Role, Greenhouse, Sensor, SensorValue, Model, ModelValue
from bluedragonfly.database.database import DB, session_scope
from bluedragonfly.database import db
import sqlalchemy
import pandas as pd
import logging
import click

FORMAT = '%(asctime)s %(levelname)s %(message)s'
logging.basicConfig(level=logging.DEBUG, format=FORMAT)

ROOT =  Path(__file__).absolute().parents[3]
DATA = ROOT / 'data'

from bluedragonfly.database import db

@click.command("create_example_db")
@click.argument('db_path', type=click.Path(exists=False))
def create_example_db(db_path):
    """
    Create an example DB with a user with role admin, owner of a greenhouse with some sensors and some data.
    """

    engine = sqlalchemy.create_engine('sqlite:///'+ db_path, echo=False)
    Session = sqlalchemy.orm.sessionmaker(bind=engine)
    logging.info('Created the engine and session.')
    Base.metadata.create_all(engine)
    logging.info('Initialized an empty database.')

    df = pd.read_csv(DATA / 'syntethic_data' / 'sensors.csv',parse_dates=True, index_col='DateTime')
    logging.info('Read synthetic data.')

    sensor_uids, sensor_units = zip(*[tuple(name.split(' ')) for name in df.columns])
    sensor_units = [unit.replace(')', '').replace('(', '') for unit in sensor_units]

    sensors = [ Sensor(uid=uid, unit=unit) for uid, unit in zip(sensor_uids, sensor_units) ]
    logging.info('Created synthetic sensors.')

    phs = Model(name='photosynthesis', unit="Unknown") # TODO specify unit.
    models = [phs]
    logging.info('Created Models.')

    gh_alpha = Greenhouse(name='gh.alpha', models=models, sensors=sensors)

    sensors_beta = [Sensor(uid='temperature', unit='C'), Sensor(uid='humidity', unit='%')]
    models_beta = [phs]
    gh_beta = Greenhouse(name='gh.beta', models=models_beta, sensors=sensors_beta)

    logging.info('Created Greenhouse.')

    admin = Role(name='Admin')
    user = User(fullname='Dummy LeTont', email='matteo.giani.87@gmail.com', password='dummy_letont', roles=[admin], greenhouses=[gh_alpha, gh_beta])
    logging.info('Created dummy user.')

    # # add all these elements to the session.
    with session_scope(Session) as session:
        session.add(user)

        logging.info('Established relationships.')

    # Reshape the dataframe to the necessary format, and add the sensor data to the db.
    N, K = df.shape
    data = {'value': df.to_numpy().ravel('F'),
            'uid': np.asarray(sensor_uids).repeat(N)}

    df1 = pd.DataFrame(data, columns=['value', 'uid'], index = np.tile(np.asarray(df.index), K))

    logging.info('Manipulated data.')

    with session_scope(Session) as session:
        DB.append_sensor_data(session, df1)

    logging.info('Added data.')

    # Computing photosynthesis.
    # General procedure: retrieving sensor data
    with session_scope(Session) as session:
        temperature = DB.retrieve_sensor_data(session, sensor_uid='sensor1.Air.T')
        rh = DB.retrieve_sensor_data(session, sensor_uid='sensor.Air.RH')
        co2 =  DB.retrieve_sensor_data(session, sensor_uid='sensor.Air.ppm')
        par =  DB.retrieve_sensor_data(session, sensor_uid='sensor.PARsensor.Above')

    df = pd.concat([temperature, rh, co2, par], axis=0).reset_index().pivot(index='datetime', columns='uid', values='value')
    from bluedragonfly.models.bio import photosynthesis

    df['photosynthesis'] = df.apply(lambda x: photosynthesis(x[0], x[1], x[2], x[3]), axis=1)

    with session_scope(Session) as session:
        phs = DB.retrieve_obj(session, Model, name='photosynthesis')
        df.apply(lambda x : phs.values.append(ModelValue(datetime=pd.to_datetime(x.name), value=x['photosynthesis'])), axis=1)
        session.add(phs)

if __name__ == "__main__":

    DB_PATH = DATA / 'test.sqlite3'
    create_example_db(DB_PATH.as_posix())