"""
Git utils.
"""

import git

def get_version():
    """
    Equivalent to git describe --tags
    """
    repo = git.Repo(search_parent_directories=True)
    return repo.git.describe('--tags')
