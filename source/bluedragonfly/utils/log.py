"""
A module to handle the application log.
"""

import logging

logger_initialized = False
def setup_custom_logger(name, log_to_file=True, log_file=None, mode='w'):
    """
    Log to the console and to file.
    """
    global logger_initialized

    if logger_initialized:
      return

    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)

    formatter = logging.Formatter(fmt='%(asctime)s - %(levelname)s - %(module)s - %(message)s')

    handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    if log_to_file:
      file = logging.FileHandler(LOG_FILE, mode=mode)
      file.setFormatter(formatter)
      logger.addHandler(file)

    logger_initialized = True

    return logger
