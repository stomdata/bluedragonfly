"""
Models for all tables in our database.
"""
import pandas as pd
import sqlalchemy
from sqlalchemy import Table, Column, Integer, DateTime, String, ForeignKey, Float, Binary, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method
from sqlalchemy.orm import relationship
from werkzeug.security import generate_password_hash, check_password_hash
from flask_user import UserMixin
from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)
from flask import current_app

# from bluedragonfly.core.db as db

Base = declarative_base()

# Sensors and greenhouses hold a one-to-many relationship.
# This is handled through foreign keys.
# User and greenhouses hold a many-to-many relationship
# This is handled in sqlalchemy through the help of a third table.
users_gh_association = Table('users_gh', Base.metadata,
    Column('user_id', Integer, ForeignKey('users.id')),
    Column('greenhouse_id', Integer, ForeignKey('greenhouses.id'))
)

class Sensor(Base):
    """
    Class for storing sensor data.
    """
    __tablename__ = 'sensors'
    id = Column(Integer, primary_key=True)
    uid = Column('uid', String(32), unique=True)
    unit = Column('unit', String(32), nullable=False)
    location = Column('location', String(32))

    greenhouse_id = Column(Integer, ForeignKey('greenhouses.id'))
    greenhouse = relationship("Greenhouse", back_populates="sensors")

    values = relationship("SensorValue", back_populates="sensor")

    @staticmethod
    def create(session, **kwargs):
        session.add(Greenhouse(**kwargs))

    def __repr__(self):
        return "<Sensor(uid='%s', unit='%s')>" % (self.uid, self.unit)

class Model(Base):
    """
    Class for storing model data.
    """
    __tablename__ = 'models'
    id = Column(Integer, primary_key=True)
    name = Column('uid', String(32), unique=True)
    unit = Column('unit', String(32), nullable=False)

    greenhouse_id = Column(Integer, ForeignKey('greenhouses.id'))
    greenhouse = relationship("Greenhouse", back_populates="models")

    values = relationship("ModelValue", back_populates="model")

    def __repr__(self):
        return "<Model(name='%s', unit='%s')>" % (self.name, self.unit)

class Value (Base):
    """
    Class for storing sensor value data.
    """
    __tablename__ = "values"
    id = Column(Integer, primary_key=True)
    datetime = Column('datetime', DateTime, nullable=False)
    value = Column('value', Float)
    type = Column(String(50))

    __mapper_args__ = {
        'polymorphic_identity':'values',
        'polymorphic_on':type
    }

    def __repr__(self):
        return "<Value(date='%s', value='%s')>" % (self.datetime, self.value)

class SensorValue(Value):

    __tablename__ = 'sensor_values '
    id = Column(Integer, ForeignKey('values.id'), primary_key=True)
    sensor_id = Column(Integer, ForeignKey('sensors.id'))
    sensor = relationship("Sensor", back_populates="values")

    __mapper_args__ = {
        'polymorphic_identity':'sensor_values',
    }

    def __repr__(self):
        return "<SValue(date='%s', value='%s')>" % (self.datetime, self.value)


class ModelValue(Value):

    __tablename__ = 'model_values '
    id = Column(Integer, ForeignKey('values.id'), primary_key=True)
    model_id = Column(Integer, ForeignKey('models.id'))
    model = relationship("Model", back_populates="values")

    __mapper_args__ = {
        'polymorphic_identity':'model_values',
    }

    def __repr__(self):
        return "<MValue(date='%s', value='%s')>" % (self.datetime, self.value)

class Greenhouse(Base):
    """
    Class for storing greenhouse data.
    """
    __tablename__ = 'greenhouses'
    id = Column(Integer, primary_key=True)
    name = Column('name', String, unique=True)
    users = relationship("User", secondary=users_gh_association, back_populates="greenhouses")
    sensors = relationship("Sensor", order_by=Sensor.id, back_populates="greenhouse")
    models = relationship("Model", order_by=Model.id, back_populates="greenhouse")

    @staticmethod
    def create(session, **kwargs):
        session.add(Greenhouse(**kwargs))

    def __repr__(self):
        return "<Greenhouse(name='%s' sensors='%s')>" % (self.name, self.sensors)

class User(Base, UserMixin):
    """
    Class for storing user data.
    """
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    email = Column('email', String, unique=True)
    fullname = Column('fullname', String)
    _password_hash = Column('password_hash', String)
    greenhouses = relationship("Greenhouse", secondary=users_gh_association, back_populates="users")
    roles = relationship('Role', secondary='user_roles')
    authenticated = Column(Boolean, default=False)

    def __repr__(self):
        return "<User(fullname='%s', email='%s', roles='%s')>" % (self.fullname, self.email, [role.name for role in self.roles])

    @hybrid_property
    def password(self):
        return self._password_hash

    @password.setter
    def password(self, plain_text_password):
        self._password_hash = generate_password_hash(plain_text_password)

    @hybrid_method
    def check_password(self, plain_text_password):
        return check_password_hash(self.password, plain_text_password)

#     def get_id(self):
#         """Return the email address to satisfy Flask-Login's requirements."""
#         return self.email

    def is_authenticated(self):
        return self.authenticated

    def generate_auth_token(self, expiration = 600):
        s = Serializer(current_app.config['SECRET_KEY'], expires_in = expiration)
        return s.dumps({ 'id': self.id })

class Role(Base):
    __tablename__ = 'roles'
    id = Column(Integer(), primary_key=True)
    name = Column(String(50), unique=True)

    def __repr__(self):
        return "<Role(name='%s')>" % (self.name)

class UserRoles(Base):
    __tablename__ = 'user_roles'
    id = Column(Integer(), primary_key=True)
    user_id = Column(Integer(), ForeignKey('users.id', ondelete='CASCADE'))
    role_id = Column(Integer(), ForeignKey('roles.id', ondelete='CASCADE'))
