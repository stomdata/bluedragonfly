"""
Database functionality.
"""

import pandas as pd
import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from contextlib import contextmanager
from pathlib import Path
import logging
import click

from .models import User, Role, Greenhouse, Sensor, Model, SensorValue, ModelValue

@contextmanager
def session_scope(Session):
    """Provide a transactional scope around a series of operations."""
    session = Session()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()

class DB():

    def __init__(self, engine=None, Session=None):
        self.engine = engine
        self.Session = Session

    def init_app(self, app):
        self.engine = sqlalchemy.create_engine(app.config['DATABASE_URI'], echo=app.config['SHOW_QUERIES'])
        self.Session = sqlalchemy.orm.sessionmaker(bind=self.engine)
        logging.info('Using db %s', app.config['DATABASE_URI'])

    @staticmethod
    def retrieve_data(session, Item, **kwargs):
        query = session.query(Item)
        for key, value in kwargs.items():
            query = query.filter(getattr(Item, key) == value)

        df = pd.read_sql(query.statement, query.session.bind)
        return df

    @staticmethod
    def retrieve_obj(session, Item, **kwargs):
        query = session.query(Item)
        for key, value in kwargs.items():
            query = query.filter(getattr(Item, key) == value)

        return query.first()

    @staticmethod
    def retrieve_sensor_data(session, sensor_uid=None, greenhouse_id=None, start=None, end=None):
        """
        Retrieves data for one sensor from the database.
        """
        query = session.query(SensorValue.datetime, SensorValue.value, Sensor.uid, Sensor.unit).filter(SensorValue.sensor_id == Sensor.id)
        if greenhouse_id:
            query = query.filter(Sensor.greenhouse_id == greenhouse_id)
        if sensor_uid:
            query = query.filter(Sensor.uid == sensor_uid)
        if start:
            query = query.filter(SensorValue.datetime >= pd.to_datetime(start))
        if end:
            query = query.filter(SensorValue.datetime <= pd.to_datetime(end))

        df = pd.read_sql(query.statement, query.session.bind, index_col='datetime')

        return df

    @staticmethod
    def retrieve_model_data(session, model_name=None, greenhouse_id=None, start=None, end=None):
        """
        Retrieves data for one model from the database.
        """
        query = session.query(ModelValue.datetime, ModelValue.value, Model.unit, Model.name).filter(ModelValue.model_id == Model.id)
        if greenhouse_id:
            query = query.filter(Model.greenhouse_id == greenhouse_id)
        if model_name:
            query = query.filter(Model.name == model_name)
        if start:
            query = query.filter(Model.datetime >= pd.to_datetime(start))
        if end:
            query = query.filter(Model.datetime <= pd.to_datetime(end))

        df = pd.read_sql(query.statement, query.session.bind, index_col='datetime')

        return df

    @staticmethod
    def append_sensor_data(session, df):
        """
        Adds data to the current, existing sensor_value table.

        This function expects a df in the form:
                    value uid
        2018-01-01   3.03  s1
        2018-01-02   3.01  s1

        Other columns (e.g. the greenhouse, or the unit) are simply ignored
        and their values not checked.
        The sensor with the specified uid must already exist.
        """
        def append_data(uid, group):
            sensor = session.query(Sensor).filter(Sensor.uid == uid).first()

            if sensor is None:
                logging.error('Sensor %s does not exist.', uid)
                return

            group.apply(lambda x : sensor.values.append(SensorValue(datetime=pd.to_datetime(x.name), value=x['value'])), axis=1)
            session.add(sensor)
            # MG I don't completely understand why this is necessary. Probably because the lambda takes a copy of the session?
            session.commit()

        df.groupby(['uid']).apply(lambda x: append_data(x.name, x))

    @staticmethod
    def retrieve_user(session, fullname=None, user_id=None, email=None):
        query = session.query(User)
        if fullname:
            query = query.filter(User.fullname == fullname)
        if user_id:
            query = query.filter(User.id == user_id)
        if email:
            query = query.filter(User.email == email)

        return query.first()

    @staticmethod
    @click.command("add_user")
    def add_user(session, fullname, email, password, roles=None, greenhouses=None):

        _roles = [DB.retrieve_obj(session=session, Item=Role, name=name) for name in roles]
        _greenhouses = [DB.retrieve_obj(session=session, Item=Greenhouse, name=name) for name in greenhouses]

        user = User(fullname=fullname,
                    email=email,
                    password=password,
                    roles=_roles,
                    greenhouses=_greenhouses)

    # # add all these elements to the session.
        session.add(user)

    @staticmethod
    @click.command("add_greenhouse")
    def add_greenhouse(session, name, users, sensors, models):
        pass
