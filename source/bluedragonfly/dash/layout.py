import dash_core_components as dcc
import dash_html_components as html

layout = html.Div([
    html.H1('Sensor Data'),
    dcc.Dropdown(
        id='my-dropdown',
        options=[
            {'label': 'CO2', 'value': 'CO2'},
            {'label': 'Temperature', 'value': 'Temperature'},
            {'label': 'RH', 'value': 'RH'}
        ],
        value='Temperature'
    ),
    dcc.Graph(id='my-graph')
], style={'width': '500'})