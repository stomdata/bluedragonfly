import dash

def register_dashapps(server):
    from bluedragonfly.dash.layout import layout
    from bluedragonfly.dash.callbacks import register_callbacks

    app = dash.Dash(name='app1',
                    server=server,
                    url_base_pathname='/app/')

    with server.app_context():
        app.title = 'BlueDragonFly'
        app.layout = layout
        register_callbacks(app)