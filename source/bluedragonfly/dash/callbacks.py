from datetime import datetime as dt

import pandas as pd
import numpy as np
from dash.dependencies import Input
from dash.dependencies import Output
import logging


def register_callbacks(dashapp):
    @dashapp.callback(Output('my-graph', 'figure'), [Input('my-dropdown', 'value')])
    def update_graph(selected_dropdown_value):

        dates = pd.date_range(start='2019-10-01', end='2019-11-01', freq='1h')
        logging.info(dates)
        df = pd.DataFrame(
            {
                'CO2' :  np.random.uniform(300,800,len(dates)),
                'Temperature' :  np.random.uniform(-10,30,len(dates)),
                'RH' :  np.random.uniform(0,100,len(dates)),
            },
            index=dates)

        return {
            'data': [{
                'x': df.index,
                'y': df[selected_dropdown_value]
            }],
            'layout': {'margin': {'l': 40, 'r': 0, 't': 20, 'b': 30}}
        }
