import numpy as np
import os

# TODO: magic constants make everything go right
def photosynthesis_iglob(T, RH, CO2, Iglob):
    CO2air_vpm = CO2 * 1.94 * 0.51
    aux = 2.2*(1-np.exp((-0.003)*Iglob))
    return (aux)/(1+230/(CO2air_vpm))*36

# TODO: magic constants make everything go right
def photosynthesis(T, RH, CO2, PAR):
    CO2air_vpm = CO2 * 1.94 * 0.51
    aux = 2.2*(1-np.exp((-0.0015)*PAR))
    return (aux)/(1+230/(CO2air_vpm))*36

