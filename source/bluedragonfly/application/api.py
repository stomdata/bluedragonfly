"""
This api allows users to retrieve and post data to/from the db.
"""

from flask import Blueprint, abort, request, jsonify
import logging
from datetime import datetime
import pandas as pd
import json

api = Blueprint('api', __name__, url_prefix='/api')

DATEFORMAT = "%Y-%m-%dT%H:%M"

from ..database import db, session_scope

@api.route('/data/', methods=['GET'])
def retrieve_data():

    # parameters
    _model = request.args.get('model')
    _sensor = request.args.get('sensor')

    if not (_sensor or _model):
      logging.error('No sensor or model specified')
      abort(500)

    _start = request.args.get('start')
    _end = request.args.get('end')
    if _start:
        _start = datetime.strptime(_start, DATEFORMAT)
    if _end:
        _end = datetime.strptime(_end, DATEFORMAT)

    with session_scope(db.Session) as session:
      if _sensor:
         df = db.retrieve_sensor_data(session=session, sensor_uid=_sensor, start=_start, end=_end)
      if _model:
        pass
        #df = db.retrieve_model_data(session=session, model_name=_model, start=_start, end=_end)

    return df.to_json(orient='index')

@api.route('/data/', methods=['POST'])
def append_data():

    df = pd.DataFrame.from_dict(json.loads(request.json), orient='index')
    df.index = pd.to_datetime(df.index, unit='ms')

    with session_scope(db.Session) as session:
        db.append_sensor_data(session, df)

    return jsonify('ok')