from jinja2 import TemplateNotFound
from flask import Blueprint, render_template

errors = Blueprint('errors', __name__, url_prefix='/', template_folder='../templates', static_folder='../static')

@errors.errorhandler(TemplateNotFound)
def template_not_found(e):
   return render_template('errors/404.html')
