"""
WIP
"""
# see https://github.com/okomarov/dash_on_flask/blob/master/app/__init__.py

import os
import logging
logging.basicConfig(level=logging.DEBUG)
import flask
import dash
import flask_login
from flask_mail import Message

from flask import Blueprint, abort
import logging

from ..utils.git import get_version

# TODO understand if the template_folder and static_folder need to be specified.
core = Blueprint('core', __name__, url_prefix='/', template_folder='../templates', static_folder='../static')

@core.route('/', defaults={'pagename': 'index'})
@core.route('/<pagename>')
def show(pagename):
    return flask.render_template(pagename+'.html', version=get_version())

@core.route('/test')
def test():
    return '<strong>It\'s Alive!</strong>'

@core.route('/mail')
def test_mail():
   msg = Message("Hello",
                  sender=("bluedragonfly", "mail@bluedragonfly.com"),
                  recipients=["matteo.giani@vortech.nl"])
   bluedragonfly.application.mail.send(msg)

   return '<strong>Mail sent!</strong>'


# TODO understand if the template_folder and static_folder need to be specified.
# auth = Blueprint('core', __name__, url_prefix='/auth', template_folder='../templates', static_folder='../static')
