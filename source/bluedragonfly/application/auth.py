from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)
from flask import current_app, Blueprint
from flask_httpauth import HTTPBasicAuth

httpauth = HTTPBasicAuth()
auth = Blueprint('auth', __name__, url_prefix='/auth')

from ..database import db, session_scope

def verify_auth_token(token, session):
    s = Serializer(current_app.config['SECRET_KEY'])
    try:
       data = s.loads(token)
    except SignatureExpired:
       return None # valid token, but expired
    except BadSignature:
       return None # invalid token
    with session_scope(db.Session) as session:
        user = db.retrieve_user(session=session, user_id=data['id'])
    return user

from flask import session, jsonify

@httpauth.verify_password
def verify_password(username_or_token, password):

    with session_scope(db.Session) as db_session:
        # first try to authenticate by token
        user = verify_auth_token(session=db_session, token=username_or_token)
        if not user:
            # try to authenticate with username/password
            user = db.retrieve_user(session=db_session, fullname=username_or_token)
            if not user or not user.check_password(password):
                return False
            session['user_id'] = user.id

    return True

@auth.route('/token')
@httpauth.login_required
def get_auth_token():
    with session_scope(db.Session) as db_session:
        user = db.retrieve_user(session=db_session, user_id=session['user_id'])
        token = user.generate_auth_token()
    return jsonify({ 'token': token.decode('ascii') })

@auth.route('/test')
@httpauth.login_required
def test_login():
    return "Success", 200
