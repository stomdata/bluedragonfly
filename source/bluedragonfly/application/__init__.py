"""
Main module to initialize the flask app.
"""
from pathlib import Path
from flask import Flask, send_from_directory, abort, render_template
from flask_debugtoolbar import DebugToolbarExtension
from flask_bootstrap import Bootstrap
from flask_mail import Mail
from flask_user import current_user, login_required, roles_required, UserManager
from flask_user import current_user, login_required, roles_required, UserManager

from .core import core
from .api import api
from .auth import auth
from .errors import errors
from ..database import db
from ..dash import register_dashapps

from ..utils.create_db import create_example_db

STATIC_FOLDER = Path("./static")
TEMPLATES_FOLDER = Path("./templates")

mail = Mail()
bootstrap = Bootstrap()

def create_app(config_obj='bluedragonfly.configuration.DevelopmentConfig'):
    """
    The create_app function builds (using a factory pattern)
    the flask application.
    """

    app = Flask(__name__)
    app.config.from_object(config_obj)

    app.cli.add_command(create_example_db)

    db.init_app(app)

    bootstrap.init_app(app)
    mail.init_app(app)

    app.register_blueprint(core)
    app.register_blueprint(api)
    app.register_blueprint(auth)
    app.register_blueprint(errors)

    # TODO
    # from yourapplication.model import db
    # db.init_app(app)

    # TODO BUG
    # when "registering" the dash app(s) within the flask app,
    # File "[...]/flask_debugtoolbar/__init__.py", line 202, in process_response
    # response_html = response.data.decode(response.charset)
    # UnicodeDecodeError: 'utf-8' codec can't decode byte 0x8b in position 1: invalid start byte
    # DebugToolbarExtension(app)
    register_dashapps(app)

    # TODO is there a better way to serve static files?
    @app.route('/<path:resource>')
    def serveStaticResource(resource):
        return send_from_directory(STATIC_FOLDER, resource)

    @app.errorhandler(500)
    def page_not_found(e):
        # return render_template((TEMPLATES_FOLDER / 'errors' / '500.html').as_posix()), 500
        return render_template('errors/500.html'), 500

    @roles_required('Admin')
    @app.route('/admin')
    def admin_page():
        return 'Admin page.'

    @login_required
    @app.route('/membrs')
    def members_page():
        return 'Members page.'

    return app
