"""
This module is used to provide the dashboard package with some basic paths.
"""

from pathlib import Path

# paths
ROOT =  Path(__file__).parent[2]

# MG make this configurable.
LOG_FILE = ROOT / "log" / "bluedragonfly.log"