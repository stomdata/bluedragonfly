
## Build the images

issue first:

`export MACHTYPE=${MACHTYPE}`

and then:

`docker-compose build`

#### Re-use the cache
The `resources/cache` directory is mounted inside of the docker image to speed up the installation of the pip packages (expecially on the RPi).
After a successfull installation, the local cache directory can be updated by copying over the compiled objects and wheels from the docker image.
To do so, use the `copy_cache.sh` script. Subsequent builds should now take less time.

## Start the application

to start the application, issue first:

`export MACHTYPE=${MACHTYPE}`

and then:

`docker-compose up -d`
