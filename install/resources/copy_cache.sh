#!/bin/bash

CONTAINER=

SRC_PATH=/root/.cache
DEST_PATH=/app/cache

echo "Executing as user $USER, uid $UID"
echo "Exporting cache from $SRC_PATH inside of the container to 'cache'"

# docker cp -r $CONTAINER:$SRC_PATH $DEST_PATH
docker run -v `pwd`:/app  registry.gitlab.com/stomdata/bluedragonfly/dashboard-${MACHTYPE} /bin/bash -c "cp -r $SRC_PATH $DEST_PATH; chown -R $UID:$UID $DEST_PATH"
