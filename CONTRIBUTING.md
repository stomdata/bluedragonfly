## Git workflow
------------
 
We use the [Gitlab workflow](https://docs.gitlab.com/ee/workflow/gitlab_flow.html) and
[Gitflow workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow).

The `master` branch contains the latest release of the code. Code is merged here from `develop` after extensive testing.

The `develop` branch contains the latest stable development code, and branches should branch off of _develop_.
The _develop_ branch will be _protected_, meaning that developers must create a merge (pull) request to merge
a feature branch into it.

We use a feature branch for each issue. As soon as a merge to _develop_ is needed, please first merge _develop_ into the feature branch,
and then assign the merge request to another developer that will then review the changes and merge the result into _develop_.

