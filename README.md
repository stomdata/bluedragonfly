## Introduction
This is the BlueDragonfly project, a decision support system (DSS) for autonomous greenhouses.

## Cloning the repository

The repository contains both source code and binary files. The different versions of the  the binary files are managed through the git command line extension [git-lfs](https://git-lfs.github.com/). In order to clone this repository, make sure to have git-lfs installed.

To clone the repository, including the most recent version of the binary files:

`git lfs clone git@gitlab.com:stomdata/bluedrangonfly.git`

The `lfs`  keyword is not strictly needed, but it will speedup the cloning process significantly.

### Clone the repository **without** any binary files

If you're just interested in the source code, not the binary files (on Linux or OSX) clone with:

`GIT_LFS_SKIP_SMUDGE=1 git clone git@gitlab.com:stomdata/bluedrangonfly.git`

This way only pointers to the binary files are added to the clone.

### Conda

Anaconda can and should be used to create an environment separate from your base one. Install the latest version of [Miniconda](https://docs.conda.io/en/latest/miniconda.html) (preferably) or [Anaconda](https://www.anaconda.com/download/#linux) (the Python 3.6 version).
The `bluedragonfly.yml` contains a set of useful packages useful in the development phase.

Create then the environment:

`conda create -f install/bluedragonfly.yml`

To activate the conda environment (miniconda):
`conda activate bluedragonfly`

See the next point for how to install the packages and their dependencies.

### Install through pip

The source code is split in two separate subpackages: _core_ and _dashboard_. Each package comes with a
`setup.py` that allows you to install it, alongside with its depencencies in your environment (e.g. a conda environment). Next to the `setup.py` there is also a `requirements.txt` that refers back to the `setup.py`. __The setup.py is then the only place where the dependencies are listed__.

To install e.g. the core package in development mode:
`cd source/core && pip install -e .`

### Docker

Additionally, we provide two docker images for the 'core' and 'dashboard' packages.
The images are meant to be used in the GitLab CI pipeline and for deployment purposes (not in development). We use docker-compose to manage both images. See [this link](https://docs.docker.com/compose/install/) to install it. The images are stored in the [container registry](https://gitlab.com/stomdata/bluedrangonfly/container_registry) of the project.

To build them:
`docker-compose build dashboard` or `docker-compose build core`

see the `Readme.md` inside of the install folder for more info on how to speed up the build process by re-using the cache, and for how to start the application.


